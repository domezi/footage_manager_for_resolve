#!/bin/bash
v=0.4.0.0

# start
clear
echo -e "======================================="
echo -e "\e[32m\e[1mFootage manager for resolve - v$v"
echo -e "send bugs to info@ziegenhagel.com"
echo "https://gitlab.com/domezi/footage_manager_for_resolve"
echo -e "\e[0m======================================="

# functions
function breakpoint {
    read -p $'\e[0m\e[32mDid I get that correct? Press return to continue, "r" to restart script or "q" to quit: ' -n1 continue 
    if [ "$continue" = "q" ]
    then 
        echo 
        echo -e "\e[31mokay, I'll exit now\e[0m"
        exit
    elif [ "$continue" = "r" ]
    then 
        exec bash "$0" "$@"
    else
        echo -e "\e[32mokay, let's move on\e[0m"
        echo
    fi
}


# start the program
echo -e "\e[2mI will now look for the most recently plugged in device...\e[0m"
dev=$(mount | tail -1)

# check if seems right
if [[ $dev == /dev/* ]]
then
    echo "Seems to be a /dev at least, continuing ..."
else
    echo "Couldn't find something. Please insert your device and make sure it's mounted."
    read -t1 -p $'\e[32mStarted watch mode, listening now ...'
    exec bash "$0" "$@"
fi

path=$(echo $dev | awk '{print $3}')
dev=$(echo $dev | awk '{print $1}')

echo -e "\e[33mThis is the device found:\e[0m"
echo "dev:              $dev"
echo "path to drive:    $path"
echo "capacity:         `du -sh $path | awk '{print $1}'`"
echo -e "largest dirs:\e[2m"
du -sh $path/* | sort -hr | head -n2
echo  "..."

# find the largest directory
dir=$path
while : 
do
    dir=$( du -sh $dir/* | sort -hr | head -1 | awk '{print $2}' )
    if [ -f $dir ]; then
        break
    fi
done
dir=$(dirname $dir)
echo -e "\e[0mlargest dir:      $dir"
echo -e "contains files like:      \e[2m"
ls -lhSr $dir | tail -4 # | tail -n +4 | head -n 3
echo  "..."

breakpoint


# now that we've found the device and location, lets ask as to where copy that stuff
add_rules=1
run_backups=0
if [ -f "$path/.backup_rules.cfg" ]
then
    echo -e "\e[33mFound the following backup rules:\e[0m"
    cat "$path/.backup_rules.cfg"
    echo

    read -n1 -e -p $'\e[0m\e[96mDo you want me to run those backup rules? Press \ns  to skip backups and rules\nd  to discard rules\na  to add rules\nq  to quit\nor return to run backups\n: \e[0m' menu 
    if [ "$menu" = "d" ]
    then
        rm "$path/.backup_rules.cfg"
        echo "rules where deleted, please add new rules now"
    elif [ "$menu" = "q" ]
    then
        echo "quitting..."
        exit
    elif [ "$menu" = "a" ]
    then
        echo "adding rules..."
    elif [ "$menu" = "s" ]
    then
        echo "skipping backups and rules..."
        run_backups=2
        add_rules=0
    else 
        echo "running backup rules..."
        run_backups=1
        add_rules=0
    fi
fi

if [ $add_rules -eq 1 ]
then
    read -e -p $'\e[0m\e[96mPlease enter a name for this device or press return to keep current name: \e[0m' newname 
    if [ "$newname" != "" ]
    then
        echo "Writing new device name ..."
        echo "display_name   $">>"$path/.backup_rules.cfg"
    fi
    echo -e "Where would you like me to backup/convert the material?"
    echo -e "\e[2me.g. /home/domi/Videos/myproject/footage or user@1.2.3.4:/opt/anotherproject/somedirectory"
    menu="n"
    while [ "$menu" = "Y" -o "$menu" = "n" ]
    do
        if [ "$menu" = "Y" ]
        then
            echo "adding another backup path ..."
        fi
        read -e -p $'\e[0m\e[96mPlease enter a backup path and press return: \e[0m' backup_path 
        echo "your path: $backup_path"
        read -n1 -e -p $'\e[0m\e[96mIs that path okay? Press \ny  to confirm and continue\nY  to confirm and add another path\nn  to type the path again \nN  to continue without the path\n: \e[0m' menu 
        if [ "$menu" = "y" -o "$menu" = "Y" ]
        then
            echo "Okay, tell me about '$backup_path' ..."
            read -n1 -e -p $'\e[0m\e[96mWhat do you want me to mark it for? Press \na  to mark for all\nb  to mark for backups\nc  to mark for conversion\n: \e[0m' submenu 
            if [ "$submenu" = "a" ]
            then
                echo -e "\e[32madding '$backup_path' to backup paths ...\e[0m"  
                echo "backup_path      $backup_path">>"$path/.backup_rules.cfg"

                echo -e "\e[32madding '$backup_path' to conversion paths ...\e[0m"  
                echo "conversion_path   $backup_path">>"$path/.backup_rules.cfg"
            elif [ "$submenu" = "b" ]
            then
                echo -e "\e[32madding '$backup_path' to backup paths ...\e[0m"  
                echo "backup_path      $backup_path">>"$path/.backup_rules.cfg"
            elif [ "$submenu" = "c" ]
            then
                echo -e "\e[32madding '$backup_path' to conversion paths ...\e[0m"  
                echo "conversion_path   $backup_path">>"$path/.backup_rules.cfg"
            fi
        fi
    done
fi


if [ $run_backups -lt 2 ]
then
    echo -e "\e[33mI'm going to run the following backup/conversion rules:\e[0m"
    cat "$path/.backup_rules.cfg"

    if [ $run_backups -eq 0 ]
    then
        breakpoint
    fi

    # actually run backups
    echo -e "\e[33mstart backing up...\e[0m"
    echo

    while read line
    do
        key=$( echo $line | awk '{ print $1 }' )
        value=$( echo $line | awk '{ print $2 }' )
        if [ "$key" = "backup_path" ]
        then
            echo -e "\e[33mstart with backup to $value\e[0m"
            mkdir -p "$value/`date '+%Y-%m-%d'`"
            rsync -hvula "$dir/" "$value/`date '+%Y-%m-%d'`"
            echo -e "\e[33mdone with backup to $value\e[0m"
            echo
        else
            echo -e "\e[33munknown $key in $path/.backup_rules.cfg\e[0m"
        fi
    done < "$path/.backup_rules.cfg"

    echo -e "\e[32mdone backing up.\e[0m"

fi

echo 

echo -e "\e[33mDo you want me to convert these files for use with DaVinci Resolve?\e[0m"
echo "(You may unplug the sd card for that)"

read -p "I'll start automatically in 5s" -t5 foo


# do the ffmpeg conversions
# find directory to do converts
while read line
do
    key=$( echo $line | awk '{ print $1 }' )
    value=$( echo $line | awk '{ print $2 }' )
    if [ "$key" = "conversion_path" ]
    then
        cd "$value/`date '+%Y-%m-%d'`"
        rm -r *.XML
        for i in *
        do
            ffmpeg -i "$i" -vcodec dnxhd -acodec pcm_s16le -s 1920x1080 -r 30000/1001 -b:v 36M -pix_fmt yuv422p -f mov "$i.mov";
            rm "$i"
        done
    else
        echo -e "\e[33munknown $key in $path/.backup_rules.cfg\e[0m"
    fi

done < "$path/.backup_rules.cfg"
