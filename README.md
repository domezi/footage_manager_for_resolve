# what it does:
run this programm to automatically copy / convert your footage from sd cards.
config files are kept on the sd cards, including your rules for automated backups and conversions

# how to set up:
running `./setup_alias_command.sh` will set up an alias in your .bashrc, so you can run this script by calling `dit`
otherwise just run it yourself: `./run.sh`

# requirements:
tested on debian, might be working on mac os as well

# dependencies
ffmpeg
rsync

# screenshots
![Startpage](https://gitlab.com/domezi/footage_manager_for_resolve/uploads/a2a930c4fdbd11a897a06ef8222f19bd/Screenshot_from_2020-05-18_11-09-12.png)